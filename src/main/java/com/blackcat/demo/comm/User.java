package com.blackcat.demo.comm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.Optional;

/**
 * <p> ：测试类User
 * @author : blackcat
 * @date : 2020/1/21 10:45
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class User{

    private String username;
    private String userPassword;
    private Address address;
    private Address2 address2;

    private String position;

    public Optional<String> getPosition() {
        return Optional.ofNullable(position);
    }

    public Optional<Address2> getAddress2() {
        return Optional.ofNullable(address2);
    }

    public User(String username, String userPassword) {
        this.username = username;
        this.userPassword = userPassword;
    }

    public void remove(){}
}



