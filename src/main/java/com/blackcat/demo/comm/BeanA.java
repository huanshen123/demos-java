package com.blackcat.demo.comm;

import com.fastobject.diff.DiffLog;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

/**
 * <p> 描述 ：对象对比示例
 * @author : blackcat
 * @date : 2021/4/27 14:24
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BeanA {

    //使用注解标记列
    @DiffLog(name = "测试a")
    private String a;

    //忽略这个
    @DiffLog(name = "测试b", ignore = true)
    private String b;

    //集合递归支持
    @DiffLog(name = "BList集合")
    private List<BeanB> bList;

    //日期格式转换
    @DiffLog(name = "开始时间",dateFormat = "yyyy-dd-MM hh:mm:ss")
    private Date start;

    @DiffLog(name = "价格")
    private BigDecimal price;

    public BeanA(String a, String b, List<BeanB> bList) {
        this.a = a;
        this.b = b;
        this.bList = bList;
    }
}
