package com.blackcat.demo.comm;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Optional;

/**
 * @Title
 * @Description
 * @author zhanghui
 * @date 2020年12月07日 15:02
 * @version V1.0
 * @see
 * @since V1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address2 {

	private Country country;

	public Optional<Country> getCountry() {
		return Optional.ofNullable(country);
	}
}
