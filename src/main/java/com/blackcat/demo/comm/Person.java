package com.blackcat.demo.comm;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * <p> 描述 ：求职者
 * @author : blackcat
 * @date : 2021/4/14 11:06
 */
@Data
@AllArgsConstructor
public class Person {
    private String name;//姓名
    private Integer age;//年龄
    private String gender;//性别
}
