package com.blackcat.demo.comm;

import com.fastobject.diff.DiffLog;
import com.fastobject.diff.DiffLogKey;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * <p> 描述 ：对象对比示例
 * @author : blackcat
 * @date : 2021/4/27 14:25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class BeanB {
    @DiffLogKey(name = "订单编号")//标记集合中对应的key,根据这个key来比对输出
    @DiffLog(name = "主键")
    private Long id ;

    @DiffLog(name = "机场")
    private String name;
}
