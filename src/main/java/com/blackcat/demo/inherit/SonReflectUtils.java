package com.blackcat.demo.inherit;


import com.blackcat.demo.comm.User;

/**
 * <p> ：继承这个父类，传入泛型的真实类型
 *
 * @author : blackcat
 * @date : 2020/1/21 13:20
 */
public class SonReflectUtils extends FatherReflectUtils<User>{
    public static void main(String[] args) {
        SonReflectUtils j = new SonReflectUtils();
        try {
            j.getTClass();
            j.getTInstance();
        }catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}
