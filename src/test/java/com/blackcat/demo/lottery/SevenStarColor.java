package com.blackcat.demo.lottery;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

/**
 * 描述 ：七星彩 彩票验证
 *
 * @author : zhangdahui
 * @date : 2022/5/3 21:36
 */
public class SevenStarColor {


    @Test
    public void test(){
        int[] winningNumbers = {4,9,0,1,7,0,5};

        List<int[]> data = new ArrayList<>();
        StringJoiner joiner = new StringJoiner(",");

        joiner.add("");

        String[] split = StringUtils.split(joiner.toString(), ',');
        for (int i = 0; i < split.length; i++) {
            int[] num = new int[7];
            num[0] = Integer.valueOf(String.valueOf(split[i].charAt(0)));
            num[1] = Integer.valueOf(String.valueOf(split[i].charAt(1)));
            num[2] = Integer.valueOf(String.valueOf(split[i].charAt(2)));
            num[3] = Integer.valueOf(String.valueOf(split[i].charAt(3)));
            num[4] = Integer.valueOf(String.valueOf(split[i].charAt(4)));
            num[5] = Integer.valueOf(String.valueOf(split[i].charAt(5)));
            if(split[i].length()==8) {
                num[6] = Integer.valueOf(String.valueOf(split[i].charAt(7)));
            }else {
                num[6] = Integer.valueOf((split[i].charAt(7))+String.valueOf(split[i].charAt(8)));
            }
            data.add(num);
        }
        data.forEach(number->{
            //查询两个数组中前6位数字相同的数量
            int count = 0;
            for (int i=0;i<number.length-1 ;i++ ){
                if(number[i]==winningNumbers[i]){
                    count++;
                }
            }
            //由相同的数字数量判断奖项
            if(number[6]==winningNumbers[6]){
                if (count==6){
                    System.out.println("恭喜您获得一等奖");
                }else if (count==5){
                    System.out.println("恭喜您获得三等奖");
                }else if (count==4){
                    System.out.println("恭喜您获得四等奖");
                }else if(count==3){
                    System.out.println("恭喜您获得五等奖");
                }else {
                    System.out.println("恭喜您获得六等奖");
                }
            }else if (count>2){
                if (count==6){
                    System.out.println("恭喜您获得二等奖");
                }else if (count==5){
                    System.out.println("恭喜您获得四等奖");
                }else if (count==4){
                    System.out.println("恭喜您获得五等奖");
                }else{
                    System.out.println("恭喜您获得六等奖");
                }
            }else {
                System.out.println("您未获奖，再接再厉");
            }
        });
    }


    /*


        joiner.add("789267-0");
        joiner.add("561769-6");
        joiner.add("549367-8");
        joiner.add("823724-11");
        joiner.add("197545-14");


        joiner.add("239793-9");
        joiner.add("506808-3");
        joiner.add("738319-0");
        joiner.add("102501-3");
        joiner.add("645261-7");

        joiner.add("614156-1");
        joiner.add("508194-10");
        joiner.add("366077-11");
        joiner.add("422550-1");
        joiner.add("346863-12");

        joiner.add("666345-11");
        joiner.add("811945-12");
        joiner.add("498257-12");
        joiner.add("256521-3");
        joiner.add("631085-7");

        joiner.add("962136-3");
        joiner.add("563708-14");
        joiner.add("046551-1");
        joiner.add("867836-9");
        joiner.add("264290-9");

        joiner.add("500315-2");
        joiner.add("504688-6");
        joiner.add("336016-13");
        joiner.add("396352-2");
        joiner.add("575430-8");

        */
}
