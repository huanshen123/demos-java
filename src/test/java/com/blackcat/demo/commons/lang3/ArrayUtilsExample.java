package com.blackcat.demo.commons.lang3;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Test;

import java.util.Arrays;

/**
 * <p> 描述 ：apache ArrayUtils工具类 使用示例
 * @author : blackcat
 * @date : 2020/2/1 8:45
 */
public class ArrayUtilsExample {
    String[] array1 = {"a","b","c"};
    String[] array2 = {"q","w","e","r","e"};
    int[] array3 = {1,2,3,1};
    Integer[] array4 = {4,5,6,7,8,9};

    @Test
    void test(){

        // 合并两个数组array1、array2，输出合并后的数组
        String[] addAll = ArrayUtils.addAll(array1, array2);
        print(addAll); //== [a, b, c, q, w, e, r, e]

        // 克隆某个数组
        String[] clone = ArrayUtils.clone(array1);
        print(clone); //== {a,b,c}

        // 将给定的数据添加到指定的数组中，返回一个新的数组。
        ArrayUtils.add(array3, 6);// {1,2,3,1,6}

        // 合并两个数组。
        ArrayUtils.addAll(array3,5,9,6,7);

        // 检查该数据在该数组中是否存在，返回一个boolean值。
        ArrayUtils.contains(array3, 9);// false

        // 合并两个数组array1、array2，输出合并后的数组
        ArrayUtils.addAll(array1, array2); //== [a, b, c, q, w, e, r, e]

        // 克隆某个数组
        ArrayUtils.clone(array1); //== {a,b,c}

        // 数组是否包含某个元素
        ArrayUtils.contains(array1, "a"); //== true

        // 获取数组的长度
        ArrayUtils.getLength(array2); //== 5

        // 从数组的第一位开始查询该数组中是否有指定的数值，存在返回index的数值，否则返回-1。有相同的取第一个
        ArrayUtils.indexOf(array2, "e"); //== 2

        // 从数组的最后一位开始往前查询该数组中是否有指定的数值，存在返回index的数值，否则返回-1。
        ArrayUtils.lastIndexOf(array3, 3); //== 2

        // 将某个数组元素倒序
        ArrayUtils.reverse(array3); //== {1,3,2,1}

        // 截取某个数组返回新的数组
        ArrayUtils.subarray(array3, 1, 2); //== {3}

//		swap():指定该数组的两个位置的元素交换或者指定两个位置后加len的长度元素进行交换。
//      nullToEmpty():
//      Insert():向指定的位置往该数组添加指定的元素，返回一个新的数组。
    }

    @Test
    void to(){
        // int[] 数组转换成 Integer[]
        ArrayUtils.toObject(array3);

        // Integer[] 数组转换成 int[]
        ArrayUtils.toPrimitive(array4);

//		toString():将数组输出为Stirng,返回一个字符串。
//		toStringArray():将Object数组转换为String数组类型。
//		toMap():将数组转换成Map,返回一个map的Object的集合。
    }

    @Test
    void is(){
        // 判断一个数组是否为空
        ArrayUtils.isEmpty(array1); //== false

        // 判断两个数组的是否相等
        ArrayUtils.isEquals(array1, array2); //== false

        // 判断两个数组的长度是否一样，当数组为空视长度为0。返回一个boolean值。
        ArrayUtils.isSameLength(array1, array2); //== false

        // 判断两个数组的类型是否一样，返回一个boolean值。
        ArrayUtils.isSameType(array1, array2); //== true

//        isSorted():判断该数组是否按照自然排列顺序排序，返回一个boolean值。
//        isNotEmpty():判断该数组是否为空，而不是null。
    }

    @Test
    void remove(){
        // 从该数组中删除指定数量的元素，返回一个新的数组。
        int[] removeElements = ArrayUtils.removeElements(array3, 1); //== {2,3,1}
//        removeElement():从该数组中删除第一次出现的指定元素，返回一个新的数组。
//        removeAllOccurences():从该数组中删除指定的元素，返回一个新的数组。
//        removeAllOccurences():从该数组中删除指定的元素，返回一个新的数组。
//        removeAll():删除指定位置上的元素，返回一个新的数组。
//        remove():删除该数组指定位置上的元素，返回一个新的数组。

    }

    public void print(Object object){
        System.out.println(ArrayUtils.toString(object));
    }

}
