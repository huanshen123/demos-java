package com.blackcat.demo.commons.collections4;

import org.apache.commons.collections4.CollectionUtils;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

/**
 * CollectionUtils 工具类使用示例
 * @author zhanghui
 * @since 2022/12/7 09:51
 */
public class CollectionUtilsExample {
    @Test
    void test(){
        String[] arrayA = new String[] { "1", "2", "3", "4"};
//        String[] arrayB = new String[] { "1", "2", "3", "4"};
        String[] arrayB = new String[] { "3", "4", "5", "6" };
        List<String> listA = Arrays.asList(arrayA);
        List<String> listB = Arrays.asList(arrayB);
        /**
         * 1、除非元素为null，否则向集合添加元素
         */
//        CollectionUtils.addIgnoreNull(personList,null);
        /**
         * 2、将两个已排序的集合a和b合并为一个已排序的列表，以便保留元素的自然顺序
         */
//        CollectionUtils.collate(Iterable<? extends O> a, Iterable<? extends O> b);
        /**
         * 3、将两个已排序的集合a和b合并到一个已排序的列表中，以便保留根据Comparator c的元素顺序。
         */
//        CollectionUtils.collate(Iterable<? extends O> a, Iterable<? extends O> b, Comparator<? super O> c)
        /**
         * 4、返回该个集合中是否含有至少有一个元素
         */
//        CollectionUtils.containsAny(Collection<?> coll1, T... coll2)
        /**
         * 5、如果参数是null，则返回不可变的空集合，否则返回参数本身。（很实用 ,最终返回List EMPTY_LIST = new EmptyList<>()）
         */
//        CollectionUtils.emptyIfNull(Collection<T> collection)
        /**
         * 6、空安全检查指定的集合是否为空
         */
//        CollectionUtils.isEmpty(Collection<?> coll)
        /**
         * 7、 空安全检查指定的集合是否为空。
         */
//        CollectionUtils.isNotEmpty(Collection<?> coll)
        /**
         * 8、反转给定数组的顺序。
         */
//        CollectionUtils.reverseArray(Object[] array);
        /**
         * 9、差集
         */
        System.out.println(CollectionUtils.subtract(listA, listB));
        //输出:[1, 2]
        /**
         * 10、并集
         */
        System.out.println(CollectionUtils.union(listA, listB));
        //输出: [1, 2, 3, 4, 5, 6]
        /**
         * 11、交集
         */
        System.out.println(CollectionUtils.intersection(listA, listB));
        //输出:[3, 4]
        /**
         *12、 交集的补集（析取）
         */
        System.out.println(CollectionUtils.disjunction(listA, listB));
        //输出:[1, 2, 5, 6]
    }

    /**
     * 差集
     */
    @Test
    void test2(){
        String[] arrayA = new String[] { "67", "1"};
        String[] arrayB = new String[] { "67", "1", "2" };
        List<String> listA = Arrays.asList(arrayA);
        List<String> listB = Arrays.asList(arrayB);
        System.out.println(CollectionUtils.subtract(listA, listB));
        // []
        System.out.println(CollectionUtils.subtract(listB, listA));
        // [2]
    }
}
