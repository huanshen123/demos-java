package com.blackcat.demo;

import org.junit.jupiter.api.Test;

/**
 * 描述 ：ServletRequestUtils使用示例
 *      request参数工具类
 * @author : blackcat
 * @date : 2021/9/16 16:13
 */
public class ServletRequestUtils {

    @Test
    public void test(){
        // 取值
//        ServletRequestUtils.getRequiredStringParameter(request, "param");
//        String param = (String) WebUtils.getSessionAttribute(request, "parameter");
        // 取值如果空给默认值
//        String param  = ServletRequestUtils.getStringParameter( request, "param", "DEFAULT");
        // 转换请求参数值至其他类型：
//        boolean param = ServletRequestUtils.getBooleanParameter( request, "param", true);
//        double param = ServletRequestUtils.getDoubleParameter( request, "param", 1000);
//        float param = ServletRequestUtils.getFloatParameter( request, "param", (float) 1.00);
//        int param = ServletRequestUtils.getIntParameter( request, "param", 100);

    }
}
