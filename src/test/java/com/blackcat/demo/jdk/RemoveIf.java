package com.blackcat.demo.jdk;

import com.blackcat.demo.comm.Person;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.function.Predicate;

/**
 * <p> 描述 ：RemoveIf示例
 * @author : blackcat
 * @date : 2021/4/14 11:03
 */
public class RemoveIf {

    // 常规写法
    @Test
    public void test(){
        Collection<Person> collection = new ArrayList();
        collection.add(new Person("张三", 22, "男"));
        collection.add(new Person("李四", 19, "女"));
        collection.add(new Person("王五", 34, "男"));
        collection.add(new Person("赵六", 30, "男"));
        collection.add(new Person("田七", 25, "女"));
        //过滤30岁以上的求职者
        Iterator<Person> iterator = collection.iterator();
        while (iterator.hasNext()) {
            Person person = iterator.next();
            if (person.getAge() >= 30)
                iterator.remove();
        }
        System.out.println(collection.toString());//查看结果
    }

    // 使用removeIf的写法 lambda表达式
    @Test
    public void test2(){
        Collection<Person> collection = new ArrayList();
        collection.add(new Person("张三", 22, "男"));
        collection.add(new Person("李四", 19, "女"));
        collection.add(new Person("王五", 34, "男"));
        collection.add(new Person("赵六", 30, "男"));
        collection.add(new Person("田七", 25, "女"));
        collection.removeIf(
                person -> person.getAge() >= 30
        );//过滤30岁以上的求职者
        System.out.println(collection.toString());//查看结果
    }

    // 使用removeIf的写法 lambda表达式
    @Test
    public void test4(){
        Collection<Person> collection = new ArrayList();
        collection.add(new Person("张三", 22, "男"));
        collection.add(new Person("李四", 19, "女"));
        collection.add(new Person("王五", 34, "男"));
        collection.add(new Person("赵六", 30, "男"));
        collection.add(new Person("田七", 25, "女"));
        collection.removeIf(
                person -> {
                    person.setName("11");
                    return person.getAge() >= 30;
                }
        );//过滤30岁以上的求职者
        System.out.println(collection.toString());//查看结果
    }

    // 使用removeIf的写法
    @Test
    public void test3(){
        Collection<Person> collection = new ArrayList();
        collection.add(new Person("张三", 22, "男"));
        collection.add(new Person("李四", 19, "女"));
        collection.add(new Person("王五", 34, "男"));
        collection.add(new Person("赵六", 30, "男"));
        collection.add(new Person("田七", 25, "女"));

        collection.removeIf(new Predicate<Person>() {
            @Override
            public boolean test(Person person) {
                return person.getAge()>=30;//过滤30岁以上的求职者
            }
        });

        System.out.println(collection.toString());//查看结果

    }

}
