package com.blackcat.demo.jdk;

import org.junit.jupiter.api.Test;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.time.temporal.ChronoField;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;

import static java.time.temporal.TemporalAdjusters.firstDayOfYear;

/**
 * <p> 描述 ：JDK8 时间类 LocalDate,LocalTime,LocalDateTime
 * @author : blackcat
 * @date : 2020/1/31 9:45
 */
public class LocalDateExample {

    @Test
    void testLocalDate(){
        // 获取当前年月日
        LocalDate localDate = LocalDate.now();
        // 构造指定的年月日
        LocalDate.of(2019, 9, 10);
        // 获取年
        localDate.getYear();
        localDate.get(ChronoField.YEAR);
        // 获取月
        localDate.getMonth();
        localDate.get(ChronoField.MONTH_OF_YEAR);
        // 获取日
        localDate.getDayOfMonth();
        localDate.get(ChronoField.DAY_OF_MONTH);
        // 获取星期几
        localDate.getDayOfWeek();
        localDate.get(ChronoField.DAY_OF_WEEK);
    }

    @Test
    void testLocalTime(){
        LocalTime localTime = LocalTime.of(13, 51, 10);
        LocalTime.now();
        // 获取小时
        localTime.getHour();
        localTime.get(ChronoField.HOUR_OF_DAY);
        // 获取分
        localTime.getMinute();
        localTime.get(ChronoField.MINUTE_OF_HOUR);
        // 获取秒
        localTime.getMinute();
        localTime.get(ChronoField.SECOND_OF_MINUTE);
    }

    @Test
    void testLocalDateTime(){
        LocalDate localDate = LocalDate.now();
        LocalTime localTime = LocalTime.of(13, 51, 10);

        // 获取年月日时分秒，等于LocalDate+LocalTime
        LocalDateTime localDateTime = LocalDateTime.now();
        LocalDateTime.of(2019, Month.SEPTEMBER, 10, 14, 46, 56);
        LocalDateTime.of(localDate, localTime);
        localDate.atTime(localTime);
        localTime.atDate(localDate);
        // 获取LocalDate
        localDateTime.toLocalDate();
        // 获取LocalTime
        localDateTime.toLocalTime();

        // 获取秒数
        // 创建Instant对象
        Instant instant = Instant.now();
        // 获取秒数
        instant.getEpochSecond();
        // 获取毫秒数
        instant.toEpochMilli();

        LocalDateTime localDateTime6 = LocalDateTime.of(
                2019, Month.SEPTEMBER, 10,
                14, 46, 56);
        //增加一年
        localDateTime6=localDateTime6.plusYears(1);
        localDateTime6=localDateTime6.plus(1, ChronoUnit.YEARS);
        //减少一个月
        localDateTime6=localDateTime6.minusMonths(1);
        localDateTime6=localDateTime6.minus(1, ChronoUnit.MONTHS);
        //修改年为2019
        localDateTime6=localDateTime6.withYear(2020);
        //修改为2022
        localDateTime6=localDateTime6.with(ChronoField.YEAR, 2022);
        System.out.println(localDateTime6);

        // 格式化时间
        localDate.with(firstDayOfYear());// 年最后一天
        LocalDate.of(2019, 9, 10);
        localDate.format(DateTimeFormatter.BASIC_ISO_DATE);
        localDate.format(DateTimeFormatter.ISO_LOCAL_DATE);
        //自定义格式化
        DateTimeFormatter dateTimeFormatter =   DateTimeFormatter.ofPattern("dd/MM/yyyy");
        localDate.format(dateTimeFormatter);
    }

    // 字符串转日期
    @Test
    void parse(){
        // LocalDateTime LocalDate LocalTime 方法类似
        LocalDate.parse("20190910", DateTimeFormatter.BASIC_ISO_DATE);
        LocalDate.parse("2019-09-10", DateTimeFormatter.ISO_LOCAL_DATE);
        LocalDateTime.parse("2019-09-10 12:49:00", DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime.parse("2015-08-04T10:11:30");
    }

    // 日期转字符串
    @Test
    void format1(){
        LocalDateTime now = LocalDateTime.now();
        System.out.println(now.format(DateTimeFormatter.ISO_LOCAL_DATE));// 2020-12-13
        System.out.println(now.format(DateTimeFormatter.ISO_DATE));// 2020-12-13
        System.out.println(now.format(DateTimeFormatter.ISO_LOCAL_TIME));// 11:54:54.596
        System.out.println(now.format(DateTimeFormatter.ISO_TIME));// 11:54:54.596
        System.out.println(now.format(DateTimeFormatter.ISO_LOCAL_DATE_TIME));// 2020-12-13T11:54:54.596
        System.out.println(now.format(DateTimeFormatter.ISO_DATE_TIME));// 2020-12-13T11:54:54.596
        System.out.println(now.format(DateTimeFormatter.ISO_ORDINAL_DATE));// 2020-348
        System.out.println(now.format(DateTimeFormatter.ISO_WEEK_DATE));// 2020-W50-7
        System.out.println(now.format(DateTimeFormatter.BASIC_ISO_DATE));// 20201213
//        String now = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
    }

    @Test
    void format2(){
        LocalDateTime of = LocalDateTime.of(2020, 12, 21, 12, 30, 59);
        //输出为：2020-12-21T12:30:59
        System.out.println(of);
        //定义格式化规则
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        //对数据进行格式化,这也是将 LocalDateTime 转为String字符串的方法
        String format = dateTimeFormatter.format(of);
        System.out.println(format);//2020/12/21 12:30:59
    }

    @Test
    void format3(){
        // 使用系统定义格式
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ISO_LOCAL_DATE_TIME;
        String format = dtf.format(now);
        //2020-09-28T11:06:44.717
        TemporalAccessor parse = dtf.parse(format);
        System.out.println(parse);
        //{},ISO resolved to 2020-09-28T11:08:05.736
    }

    @Test
    void format4(){
        // 设置为本地化相关格式
        //ofLocalizedDateTime(FormatStyle.XXX(LONG/MEDIUN/SHORT))
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT);
        String format = dtf.format(now);
        //LONG：2020年9月28日 上午11时16分45秒
        //MEDIUN: 2020-9-28 11:17:06
        //SHORT: 20-9-28 上午11:17

        TemporalAccessor parse = dtf.parse(format);
        System.out.println(parse);
        //{},ISO resolved to 2020-09-28T11:18
    }

    @Test
    void format5(){
        // 自定义格式
        LocalDateTime now = LocalDateTime.now();
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        String format = dtf.format(now);
        System.out.println(format);
        // 2020-09-28 11:40:13

        TemporalAccessor parse3 = dtf.parse("2019-07-22 03:02:08");
        System.out.println(parse3);
        // {},ISO resolved to 2019-07-22T03:02:08
    }

    // 转换为时区
    @Test
    void atZone(){
        // 将LocalDateTime转换为时区ISO8601字符串
        LocalDateTime ldt = LocalDateTime.now();
        ZonedDateTime zdt = ldt.atZone(ZoneOffset.UTC); //you might use a different zone
        String iso8601 = zdt.toString();
        System.out.println(iso8601);
    }

    @Test
    void ZonedDateTime(){
        // 从ISO8601字符串转换回LocalDateTime
        String iso8601 = "2016-02-14T18:32:04.150Z";
        ZonedDateTime zdt = ZonedDateTime.parse(iso8601);
        LocalDateTime ldt = zdt.toLocalDateTime();
        System.out.println(ldt);
    }

}
