package com.blackcat.demo.jdk;

import org.junit.jupiter.api.Test;

import java.util.StringJoiner;

/**
 * <p> 描述 ：StringJoiner 使用示例
 * @author : blackcat
 * @date : 2021/4/12 16:21
 */
public class StringJoinerTest {

    @Test
    public void test(){
        StringJoiner sj = new StringJoiner(":", "[", "]");
        sj.add("George").add("Sally").add("Fred");
        // [George:Sally:Fred]
        System.out.println(sj);
    }

    @Test
    public void test1(){
        StringJoiner sj = new StringJoiner(",");
        sj.add("George").add("Sally").add("Fred");
        // George,Sally,Fred
        System.out.println(sj);
    }

    @Test
    public void test2(){
        StringJoiner sj = new StringJoiner("','", "'", "'");
        sj.add("George").add("Sally").add("Fred");
        // 'George','Sally','Fred'
        System.out.println(sj);
    }

    @Test
    public void test3(){
        StringJoiner sj = new StringJoiner(" and ");
        sj.add("(11=22 and 33=44)").add("(21=22 and 23=44)").add("(31=22 and 33=44)");
        // (11=22 and 33=44) and (21=22 and 23=44) and (31=22 and 33=44)
        System.out.println(sj);
    }
}
