package com.blackcat.demo.colincatsu;

import com.blackcat.demo.comm.BeanA;
import com.blackcat.demo.comm.BeanB;
import com.fastobject.diff.AbstractObjectDiff;
import com.fastobject.diff.ChineseObjectDiff;
import com.fastobject.diff.DiffUtils;
import com.fastobject.diff.DiffWapper;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p> 描述 ：对象对比工具
 *      对比java相同对象内不同值的方法
 * @author : blackcat
 * @date : 2021/4/27 14:22
 */
public class AbstractObjectDiffTest {

    @Test
    public void test() throws Exception {
        BeanB a1b = new BeanB(1L,"北京");
        BeanB a1b3 = new BeanB(3L,"3");
        BeanB a1b2 = new BeanB(2L,"1");

        ArrayList<BeanB> list = new ArrayList<>();
        list.add(a1b);
        list.add(a1b3);
        list.add(a1b2);
        BeanA a1 = new BeanA("1","1",list);
        a1.setStart(new Date());
        //        a1.setPrice(new BigDecimal("10.23"));


        BeanB a2b = new BeanB(1L,"上海");
        BeanB a2b2 = new BeanB(2L,"2");

        ArrayList<BeanB> list2 = new ArrayList<>();
        list2.add(a2b);
        list2.add(a2b2);
        final BeanA a2 = new BeanA("2","2",list2);
        a2.setPrice(new BigDecimal("50.852236"));
        // 获取集合
        List<DiffWapper> diffWappers = AbstractObjectDiff.generateDiff(a1, a2);
        diffWappers.forEach(i->{
            System.out.println(i.getLogName());
            System.out.print("修改前：" + i.getDiffValue().getOldValue());
            System.out.println("  修改后：" + i.getDiffValue().getNewValue());
            System.out.println("执行操作："+i.getOp());
        });
        // 将集合输出字符串
        System.out.println(DiffUtils.genDiffStr(diffWappers));

        // 直接输出字符串
        String result = ChineseObjectDiff.genChineseDiffStr(a1, a2);
        System.out.println(result);
    }
}
