package com.blackcat.demo.jvm.newObject;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Title
 * @Description
 * @author zhanghui
 * @date 2020年12月12日 12:10
 * @version V1.0
 * @see
 * @since V1.0
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {
	private String name;
	private int age;
	private double salary;
}
