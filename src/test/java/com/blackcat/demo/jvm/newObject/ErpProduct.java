package com.blackcat.demo.jvm.newObject;

import lombok.Data;

import java.io.Serializable;

/**
 * @Title
 * @Description
 * @author zhanghui
 * @date 2020年12月12日 16:57
 * @version V1.0
 * @see
 * @since V1.0
 */
@Data
public class ErpProduct implements Serializable,Cloneable{
	private static final long serialVersionUID = 1L;
	private static ErpProduct erpProduct = new ErpProduct();

	private String topicNum;//选题号 选填
	private String compName;//部件名 必填 (多个部件以 ','分隔   封面,正文)
	private String printShop;//印厂名 必填
	private String printUser; //分发人 必填
	private String reback;//是否撤回 必填  0 默认分发  1撤回分发
	private String printNum;//印数 （选填）

	public ErpProduct() {
		super();
	}

	/**
	 * 调用对象创建优化
	 */
	public static ErpProduct getInstance(){
		try {
			return (ErpProduct) erpProduct.clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return new ErpProduct();
	}
}
