package com.blackcat.demo.jvm.newObject;

import com.blackcat.demo.jvm.newObject.Employee;
import com.blackcat.demo.jvm.newObject.ErpProduct;
import org.junit.jupiter.api.Test;

import java.util.Random;

/**
 * @Title new对象测试
 * @Description
 * @author zhanghui
 * @date 2020年12月12日 12:11
 * @version V1.0
 * @see
 * @since V1.0
 */
public class NewTest {
	private static Random rand = new Random();

	// 优化前
	public static Employee getEmployee(){
		int age = rand.nextInt(100);
		double salary = 1000.0 + rand.nextDouble() * 10000;
		// generate name: 3-5 characters
		int nameLen = 3 + rand.nextInt(3);
		StringBuilder nameBuilder = new StringBuilder();
		for(int i=0; i<nameLen; i++){
			nameBuilder.append((char)('a' + rand.nextInt(26)));
		}
		return new Employee(nameBuilder.toString(), age, salary);
	}

	// 优化后
	private static Employee employee = new Employee("", 0, 0);
	public static Employee getEmployee2(){
		int age = rand.nextInt(100);
		double salary = 1000.0 + rand.nextDouble() * 10000;
		// generate name: 3-5 characters
		int nameLen = 3 + rand.nextInt(3);
		StringBuilder nameBuilder = new StringBuilder();
		for(int i=0; i<nameLen; i++){
			nameBuilder.append((char)('a' + rand.nextInt(26)));
		}
		employee.setAge(age);
		employee.setName(nameBuilder.toString());
		employee.setSalary(salary);
		return employee;
	}

	// 通过静态变量employee每次set对象的值模拟复用，而不是new新对象。
	@Test
	void test(){
		long s = System.currentTimeMillis();
		int count = 200000;
		for(int i=0; i<count; i++){
//			Employee e = getEmployee2();
			Employee e = getEmployee();
		}
		System.out.println(System.currentTimeMillis() - s);
	}

	// 类实现Cloneable接口,利用clone。
	@Test
	void test2(){
		long beginTime = System.currentTimeMillis();

		for (int i = 0; i < 100000; i++) {
			ErpProduct.getInstance();
		}

		long endTime = System.currentTimeMillis();
		System.out.println("采用clone的方法，一共花费的时间：" + (endTime - beginTime));

		beginTime = System.currentTimeMillis();

		for (int i = 0; i < 100000; i++) {
			new ErpProduct();
		}

		endTime = System.currentTimeMillis();
		System.out.println("采用new的方法，一共花费的时间：" + (endTime - beginTime));
	}


}
